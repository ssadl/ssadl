﻿using System;

using SSADL.CMS;


public partial class UserControls_UtilityNav : System.Web.UI.UserControl
{  
    /// <summary>
    /// page load event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //get different search links content
            long logoCId = ConfigHelper.GetValueLong("SiteLogoTitleCId");
            if (logoCId > 0)
            {
                var contentData = ContentHelper.GetContentById(logoCId);
                if (contentData != null)
                    ltrLogoText.Text = contentData.Html;
            }
        }
    }  
           
}