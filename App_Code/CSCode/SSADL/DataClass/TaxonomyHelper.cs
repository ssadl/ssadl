﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ektron.Cms;
using System.Configuration;
using Ektron.Cms.Organization;
using Ektron.Cms.Framework.Organization;
using Ektron.Cms.Common;
using Ektron.Cms.API.Content;
using Ektron.Cms.Instrumentation;

namespace SSADL.CMS
{
    /// <summary>
    /// Summary description for TaxonomyHelper
    /// </summary>
    public class TaxonomyHelper
    {
        public static TaxonomyManager taxonomyManager = new TaxonomyManager(Ektron.Cms.Framework.ApiAccessMode.Admin);
        public static TaxonomyItemManager taxonomyItemManager = new TaxonomyItemManager(Ektron.Cms.Framework.ApiAccessMode.Admin);

        public TaxonomyHelper()
        {
            //constructor
        }

        /// <summary>
        /// TaxonomySortOrder 
        /// </summary>
        public enum TaxonomySortOrder
        {            
            DateCreated,
            DateModified,
            Title            
        }

        /// <summary>
        /// This method is used to get taxonomy tree by Id
        /// </summary>
        /// <param name="taxonomyId">taxonomy id</param>
        /// <param name="depth">taxonomy sub levels</param>
        /// <param name="includeItems">include content</param>
        /// <returns>TaxonomyData</returns>
        public static TaxonomyData GetTaxonomyTree(long taxonomyId, Int32 depth, bool includeItems, bool refreshCache = false)
        {            
            try
            {
                String cacheKey = String.Format("SSADL:TaxonomyId={0}:Depth={1}:IncludeItems={2}:GetTaxonomyTree:CacheBase", taxonomyId, depth, includeItems);
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (TaxonomyData)HttpRuntime.Cache[cacheKey];

                TaxonomyData taxonomyData = taxonomyManager.GetTree(taxonomyId, depth, includeItems, new PagingInfo(), Ektron.Cms.Common.EkEnumeration.TaxonomyType.Content, EkEnumeration.TaxonomyItemsSortOrder.taxonomy_item_display_order);
                if (taxonomyData != null && taxonomyData.Id > 0)
                {
                    ApplicationCache.Insert<TaxonomyData>(cacheKey, taxonomyData, CacheDuration.For12Hr);
                }
                return taxonomyData;
            }
            catch (Exception ex)
            {
                Log.WriteError("TaxonomyHelper > GetTaxonomyTree exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// This method is used to get the taxonomy data only with out content html
        /// </summary>
        /// <param name="taxonomyId">user selected taxonomy Id</param>
        /// <param name="TaxonomySortOrder">the order content should be read from db</param>
        /// <param name="cache">is caching required</param>
        /// <returns>TaxonomyData object</returns>
        public static List<TaxonomyItemData> GetTaxonomyItemDataById(long taxonomyId, TaxonomySortOrder orderType = TaxonomySortOrder.DateModified, bool refreshCache = false)
        {
            try
            {
                String cacheKey = String.Format("SSADL:TaxonomyId={0}:TaxonomySortOrder={1}:GetTaxonomyItemDataById:CacheBase", taxonomyId, orderType);
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<TaxonomyItemData>)HttpRuntime.Cache[cacheKey];

                TaxonomyItemCriteria criteria = new TaxonomyItemCriteria();
                criteria.AddFilter(TaxonomyItemProperty.TaxonomyId, CriteriaFilterOperator.EqualTo, taxonomyId);
                criteria.OrderByDirection = EkEnumeration.OrderByDirection.Descending;

                switch (orderType)
                {
                    case TaxonomySortOrder.DateCreated:
                        criteria.OrderByField = TaxonomyItemProperty.DateCreated;
                        break;
                    case TaxonomySortOrder.DateModified:
                        criteria.OrderByField = TaxonomyItemProperty.DateModified;
                        break;
                    case TaxonomySortOrder.Title:
                        criteria.OrderByField = TaxonomyItemProperty.Title;
                        break;
                    default:
                        criteria.OrderByField = TaxonomyItemProperty.Title;
                        break;
                }


                var taxonomyItemList = taxonomyItemManager.GetList(criteria);
                if (taxonomyItemList != null && taxonomyItemList.Any())
                {
                    ApplicationCache.Insert<List<TaxonomyItemData>>(cacheKey, taxonomyItemList, CacheDuration.For12Hr);
                }
                return taxonomyItemList;
            }
            catch (Exception ex)
            {
                Log.WriteError("TaxonomyHelper > GetTaxonomyItemDataById exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }


        /// <summary>
        /// This method is used to get taxonomy by Id
        /// </summary>
        /// <param name="taxonomyId">taxonomy id</param>
        /// <returns>taxonomy data</returns>
        public static TaxonomyData GetItem(long taxonomyId, bool refreshCache = false)
        {           
            try
            {
                String cacheKey = String.Format("SSADL:TaxonomyId={0}:GetItem:CacheBase", taxonomyId);
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (TaxonomyData)HttpRuntime.Cache[cacheKey];

                TaxonomyData taxonomyData = taxonomyManager.GetItem(taxonomyId);
                if (taxonomyData != null && taxonomyData.Id > 0)
                {
                    ApplicationCache.Insert<TaxonomyData>(cacheKey, taxonomyData, CacheDuration.For12Hr);
                }
                return taxonomyData;
            }
            catch (Exception ex)
            {
                Log.WriteError("TaxonomyHelper > GetItem exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }


        /// <summary>
        /// This method is used to get taxonomy data by criteria object
        /// </summary>
        /// <param name="taxonomyCriteria">TaxonomyCriteria obj</param>
        /// <returns>taxonomyData as list</returns>
        public static List<TaxonomyData> GetTaxonomyData(TaxonomyCriteria taxonomyCriteria, bool refreshCache = false)
        {            
            try
            {
                String cacheKey = String.Format("SSADL:TaxonomyCriteria={0}:GetTaxonomyData:CacheBase", taxonomyCriteria.ToCacheKey());
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<TaxonomyData>)HttpRuntime.Cache[cacheKey];

                var dataList = taxonomyManager.GetList(taxonomyCriteria);
                if (dataList != null && dataList.Any())
                {
                    ApplicationCache.Insert<List<TaxonomyData>>(cacheKey, dataList, CacheDuration.For12Hr);
                }
                return dataList;
            }
            catch (Exception ex)
            {
                Log.WriteError("TaxonomyHelper > GetTaxonomyData exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        } 
       

        /// <summary>
        /// This method is used to get the Taxonomy name for a given content item
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public static string GetTaxonomyNameByContentId(long contentId, bool refreshCache = false)
        {
            string taxonomyName = string.Empty;
            if (contentId > 0)
            {
                string cacheKey = "SSADL-GetTaxonomyNameByContentId" + contentId;
                bool dataExistInCache = ApplicationCache.IsExist(cacheKey);

                if (!dataExistInCache || refreshCache)
                {
                    Taxonomy taxonomyAPI = new Taxonomy();
                    var taxonomyBaseData = taxonomyAPI.ReadAllAssignedCategory(contentId);
                    if (taxonomyBaseData != null && taxonomyBaseData.Length > 0)
                    {
                        taxonomyName = taxonomyBaseData[0].Name;
                        ApplicationCache.Insert(cacheKey, taxonomyName, ConfigHelper.GetValueLong("longCacheInterval"));
                    }
                }
                else
                {
                    var cacheData = ApplicationCache.GetValue(cacheKey);
                    if (cacheData != null)
                        taxonomyName = (string)cacheData;
                }
            }
            return taxonomyName;
        }
        
        /// <summary>
        /// This method is used to get the Taxonomy id by passing the assigned content Id.
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public static long GetTaxonomyIdByContentId(long contentId)
        {
            long taxonomyId = 0;
            if (contentId > 0)
            {
                Taxonomy taxonomyAPI = new Taxonomy();
                var taxonomyBaseData = taxonomyAPI.ReadAllAssignedCategory(contentId);
                if (taxonomyBaseData != null && taxonomyBaseData.Length > 0)
                {
                    taxonomyId = taxonomyBaseData[0].Id;
                }
            }
            return taxonomyId;
        }    
    
    }
}