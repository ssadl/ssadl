﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using Ektron.Cms.Instrumentation;
using Ektron.Cms.Framework.Content;

namespace SSADL.CMS
{
    /// <summary>
    /// Summary description for ContentHelper
    /// </summary>
    public class ContentHelper
    {
        private static ContentManager contentManager = new ContentManager(Ektron.Cms.Framework.ApiAccessMode.Admin);

        public ContentHelper()
        {
            //constructor
        }

        /// <summary>
        /// TaxonomySortOrder 
        /// </summary>
        public enum ContentSortOrder
        {
            DateCreated,
            DateModified,
            Title
        }
        
        /// <summary>
        /// This method is used to get the content by id
        /// </summary>
        /// <param name="id">content or page id</param>
        /// <param name="returnMetaData">metadata</param>
        /// <returns>ContentData</returns>
        public static ContentData GetContentById(long contentId, bool returnMetaData = false, bool refreshCache = false)
        {
            try
            {
                String cacheKey = String.Format("SSADL:ContentID={0}:IncludeMetaData={1}:GetContentById:CacheBase", contentId, returnMetaData);
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (ContentData)HttpRuntime.Cache[cacheKey];

                ContentData cdata = contentManager.GetItem(contentId, returnMetaData);
                if (cdata != null && cdata.Id > 0)
                {
                    ApplicationCache.Insert<ContentData>(cacheKey, cdata, CacheDuration.For12Hr);
                }
                return cdata;
            }
            catch (Exception ex)
            {
                Log.WriteError("ContentHelper > GetContentById exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// This method is used to get content by ids
        /// </summary>
        /// <param name="ids">content ids</param>
        /// <param name="smartFormId">optional parameter smartformId</param>
        /// <param name="returnMetaData">optional parameter retturn metadata</param>
        /// <returns>ContentData list</returns>
        public static List<ContentData> GetContentByIds(List<long> ids, long smartFormId = 0, bool returnMetaData = false, bool refreshCache = false)
        {
            try
            {
                string contentIds = "";
                foreach (var id in ids)
                    contentIds += id.ToString();

                String cacheKey = String.Format("SSADL:ContentIDs={0}:SmartFormId={1}:IncludeMetaData={2}:GetContentByIds:CacheBase", contentIds, smartFormId, returnMetaData);
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentData>)HttpRuntime.Cache[cacheKey];

                ContentCriteria contentCriteria = new ContentCriteria();
                contentCriteria.AddFilter(ContentProperty.Id, CriteriaFilterOperator.In, ids);
                if (smartFormId > 0)
                    contentCriteria.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, smartFormId);
                if (returnMetaData)
                    contentCriteria.ReturnMetadata = true;
                var dataList = contentManager.GetList(contentCriteria);
                if (dataList != null && dataList.Any())
                {
                    ApplicationCache.Insert<List<ContentData>>(cacheKey, dataList, CacheDuration.For12Hr);
                }
                return dataList;
            }
            catch (Exception ex)
            {
                Log.WriteError("ContentHelper > GetContentByIds exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// This method is used to get content data list by content criteria
        /// </summary>
        /// <param name="criteria">ContentCriteria</param>
        /// <returns>ContentData list</returns>
        public static List<ContentData> GetListByCriteria(ContentCriteria criteria, bool refreshCache = false)
        {           
            try
            {
                String cacheKey = String.Format("SSADL:ContentCriteria={0}:GetListByCriteria:CacheBase", criteria.ToCacheKey());
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentData>)HttpRuntime.Cache[cacheKey];

                List<ContentData> dataList = contentManager.GetList(criteria);
                if (dataList != null && dataList.Any())
                {
                    ApplicationCache.Insert<List<ContentData>>(cacheKey, dataList, CacheDuration.For12Hr);
                }
                return dataList;
            }
            catch (Exception ex)
            {
                Log.WriteError("ContentHelper > GetListByCriteria exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }
        
        /// <summary>
        /// This method is used to get content data list by content criteria
        /// </summary>
        /// <param name="criteria">ContentCriteria</param>
        /// <returns>ContentData list</returns>
        public static List<ContentData> GetListByCriteria(ContentTaxonomyCriteria taxCriteria, bool refreshCache = false)
        {           
            try
            {
                String cacheKey = String.Format("SSADL:ContentTaxonomyCriteria={0}:GetListByCriteria:CacheBase", taxCriteria.ToCacheKey());
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentData>)HttpRuntime.Cache[cacheKey];

                List<ContentData> dataList = contentManager.GetList(taxCriteria);
                if (dataList != null && dataList.Any())
                {
                    ApplicationCache.Insert<List<ContentData>>(cacheKey, dataList, CacheDuration.For12Hr);
                }
                return dataList;
            }
            catch (Exception ex)
            {
                Log.WriteError("ContentHelper > GetListByCriteria exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }
        
        /// <summary>
        /// This method is used to get content data list by content criteria
        /// </summary>
        /// <param name="criteria">ContentCriteria</param>
        /// <returns>ContentData list</returns>
        public static List<ContentData> GetListByCriteria(ContentCollectionCriteria collectionCriteria, bool refreshCache = false)
        {           
            try
            {
                String cacheKey = String.Format("SSADL:ContentCollectionCriteria={0}:GetListByCriteria:CacheBase", collectionCriteria.ToCacheKey());
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentData>)HttpRuntime.Cache[cacheKey];

                List<ContentData> dataList = contentManager.GetList(collectionCriteria);
                if (dataList != null && dataList.Any())
                {
                    ApplicationCache.Insert<List<ContentData>>(cacheKey, dataList, CacheDuration.For12Hr);
                }
                return dataList;
            }
            catch (Exception ex)
            {
                Log.WriteError("ContentHelper > GetListByCriteria exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }
        
        /// <summary>
        /// This method is used to get content data list by content meta criteria
        /// </summary>
        /// <param name="criteria">ContentMetadataCriteria</param>
        /// <returns>ContentData list</returns>
        public static List<ContentData> GetListByCriteria(ContentMetadataCriteria criteria, bool refreshCache = false)
        {           
            try
            {
                String cacheKey = String.Format("SSADL:ContentMetadataCriteria={0}:GetListByCriteria:CacheBase", criteria.ToCacheKey());
                if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentData>)HttpRuntime.Cache[cacheKey];

                List<ContentData> dataList = contentManager.GetList(criteria);
                if (dataList != null && dataList.Any())
                {
                    ApplicationCache.Insert<List<ContentData>>(cacheKey, dataList, CacheDuration.For12Hr);
                }
                return dataList;
            }
            catch (Exception ex)
            {
                Log.WriteError("ContentHelper > GetListByCriteria exception!: " + ex + " :: " + ex.StackTrace);
                return null;
            }
        }        

        /// <summary>
        /// This method is used to assign content item to taxonomy
        /// </summary>
        /// <param name="contentId">content id</param>
        /// <param name="taxonomyId">taxonomy id</param>
        public static void AssignTaxonomy(long contentId, long taxonomyId)
        {
            if (contentId > 0 && taxonomyId > 0)
                contentManager.AssignTaxonomy(contentId, taxonomyId);           
        }          

        /// <summary>
        /// This method is used to get the asset path
        /// </summary>
        /// <param name="cData">asset content data</param>
        /// <returns>string asset path</returns>
        public static string GetAssetPath(ContentData cData)
        {
            string path = "";
            if (cData != null)
            {
                Ektron.Cms.API.Content.Content contentAPI = new Ektron.Cms.API.Content.Content();
                path = contentAPI.EkContentRef.GetViewUrl(cData.ContType, cData.AssetData.Id);//.Replace(Page.Request.Url.Scheme + "://" + Page.Request.Url.Authority, "");
                try
                {
                    if (path != "" && path.ToLower().Contains("assets"))
                        path = path.Remove(0, path.IndexOf("/assets"));
                }
                catch (Exception ex) 
                {
                    Log.WriteError(ex);
                    return string.Empty;
                }
            }
            return path;
        }       

    }
}