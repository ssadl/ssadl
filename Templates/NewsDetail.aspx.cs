﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SSADL.CMS;

public partial class Templates_NewsDetail : PageBase
{
    /// <summary>
    /// Page load event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                long contentId = EktUtility.ParseLong(Request.QueryString["id"]);
                this.GetNewsData(contentId);
            }
        }
        RightSideContent.ccontentID = mainContent.EkItem.Id.ToString();
        RightSideContent.cfolderID = mainContent.EkItem.FolderId.ToString();
        uxPageTitle.pgTitle = mainContent.EkItem.Title.ToString();
        uxPageTitle.pageId = mainContent.EkItem.Id.ToString();
        mainContent.Text = "<!-- -->";
        
        
        
    }

    /// <summary>
    /// This method is used to get news article data
    /// </summary>
    /// <param name="contentId"></param>
    private void GetNewsData(long contentId)
    {
        if(contentId > 0)
        {
            var newsData = SiteDataManager.GetNewsById(contentId);
            if(newsData != null && newsData.SmartForm != null)
            {
                if(newsData.SmartForm.Image != null && newsData.SmartForm.Image.img != null)
                {
                    string imgsrc = newsData.SmartForm.Image.img.src.Trim();
                    if (imgsrc != "")
                    {
                        imgDiv.Visible = true;
                        ltrNewsImg.Text = "<img title=\"" + newsData.SmartForm.Image.img.alt + "\" alt=\"" + newsData.SmartForm.Image.img.alt + "\" src=\"" + newsData.SmartForm.Image.img.src + "\" />";
                    }
                }
                ltrNewsBody.Text = EktUtility.GetHTMLNodesFromEktRich(newsData.SmartForm.FullDescription);
            }
        }
    }

}