﻿using System;
using System.Configuration;
using System.Net;
using System.Text;
using System.Xml.Linq;
using System.Net.Mail;
using Ektron.Cms.Instrumentation;


namespace SSADL.CMS
{
    /// <summary>
    /// Summary description for Utility
    /// </summary>
    public class EktUtility
    {
        public EktUtility()
        {
            //
            // TODO: Add constructor logic here
            //
        }
		
        /// <summary>
        /// Extract HTML nodes from XElement
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
		public static string ExtractNodeHtml(XElement node)
        {
            StringBuilder innerXml = new StringBuilder();
            if (node != null)
            {
                foreach (XNode n in node.Nodes())
                {
                    innerXml.Append(n.ToString());
                }
            }
            return innerXml.ToString();
        }

        /// <summary>
        /// This method is used to extract HTML nodes from rich class
        /// </summary>
        /// <param name="ektronRichTextData"></param>
        /// <returns></returns>
        public static string GetHTMLNodesFromEktRich(rich ektronRichTextData)
        {
            StringBuilder html = new StringBuilder();
            if (ektronRichTextData != null && ektronRichTextData.Any != null)
            {
                foreach (var n in ektronRichTextData.Any)
                {
                    html.Append(n.OuterXml);
                }
            }
            return html.ToString();
        }

        /// <summary>
        /// This method is used to send email
        /// </summary>
        /// <param name="toAddress">toAddress required</param>
        /// <param name="fromAddress">fromAddress required</param>
        /// <param name="ccAddress">ccAddress optional</param>
        /// <param name="emailBody">emailBody required</param>
        /// <param name="emailSubject">emailSubject required</param>
        public static void SendEmail(string toAddress, string fromAddress, string ccAddress, string emailBody, string emailSubject, string bccAddress = "")
        {
            try
            {
                if ((string.IsNullOrEmpty(toAddress)) ||
                    (string.IsNullOrEmpty(fromAddress)) ||
                    (string.IsNullOrEmpty(emailSubject)) ||
                    (emailBody == null))
                {
                    return;
                }

                string SMTPServer = ConfigurationManager.AppSettings["ek_SMTPServer"];
                string SMTPPort = ConfigurationManager.AppSettings["ek_SMTPPort"];
                string SMTPUser = ConfigurationManager.AppSettings["ek_SMTPUser"];
                string SMTPPass = ConfigurationManager.AppSettings["ek_SMTPPass"];
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.Subject = emailSubject;
                    mailMessage.From = new MailAddress(fromAddress, "SSADL");
                    string[] toarray = toAddress.Split(';');
                    foreach (string address in toarray)
                    {
                        mailMessage.To.Add(address);
                    }
                    if (!string.IsNullOrEmpty(ccAddress))
                        mailMessage.CC.Add(ccAddress);
                    if (!string.IsNullOrEmpty(bccAddress))
                        mailMessage.Bcc.Add(bccAddress);
                    mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                    mailMessage.Body = emailBody;
                    mailMessage.IsBodyHtml = true;

                    NetworkCredential networkCred = new NetworkCredential();
                    networkCred.UserName = SMTPUser;
                    networkCred.Password = SMTPPass;

                    using (SmtpClient smtpClient = new SmtpClient())
                    {
                        smtpClient.Host = SMTPServer;
                        smtpClient.Credentials = networkCred;
                        smtpClient.Port = int.Parse(SMTPPort);
                        smtpClient.Send(mailMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Ektron.Cms.EkException.WriteToEventLog(ex.Message + " : " + ex.StackTrace, System.Diagnostics.EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// this method is used to get custom page size and overwrite the default Ektron page size of 50
        /// </summary>
        /// <returns>int page size</returns>
        public static int GetCustomApiPageSize()
        {
            int pageSize = 50; //default
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["EktronContentApiCustomPageSize"], out pageSize);
            }
            catch (Exception ex)
            {
                Log.WriteError(ex);
            }
            return pageSize;
        }

        /// <summary>
        /// Helper method to parse a long value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>long value</returns>
        public static long ParseLong(string value)
        {
            long longValue = 0;
            try
            {
                long.TryParse(value, out longValue);
            }
            catch (Exception ex)
            {
                Log.WriteError(ex);
            }
            return longValue;
        }

        /// <summary>
        /// Helper method to parse a int32 value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>int32 value</returns>
        public static int ParseInt(string value)
        {
            int intValue = 0;
            try
            {
                int.TryParse(value, out intValue);
            }
            catch (Exception ex)
            {
                Log.WriteError(ex);
            }
            return intValue;
        }

        /// <summary>
        /// Helper method to parse a float value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>float value</returns>
        public static float ParseFloat(string value)
        {
            float floatValue = 0;
            try
            {
                float.TryParse(value, out floatValue);
            }
            catch (Exception ex)
            {
                Log.WriteError(ex);
            }
            return floatValue;
        }
        
        /// <summary>
        /// Helper method to parse a decimal value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>decimal value</returns>
        public static decimal ParseDecimal(string value)
        {
            decimal decimalValue = 0;
            try
            {
                decimal.TryParse(value, out decimalValue);
            }
            catch (Exception ex)
            {
                Log.WriteError(ex);
            }
            return decimalValue;
        }
        
        /// <summary>
        /// Helper method to parse a DateTime value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>DateTime value</returns>
        public static DateTime ParseDateTime(string value)
        {
            DateTime dateValue = DateTime.MinValue;
            try
            {
                DateTime.TryParse(value, out dateValue);
            }
            catch (Exception ex)
            {
                Log.WriteError(ex);
            }
            return dateValue;
        }

        /// <summary>
        /// Helper method to parse a double value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>double value</returns>
        public static double ParseDouble(string value)
        {
            double doubleValue = 0;
            try
            {
                double.TryParse(value, out doubleValue);
            }
            catch (Exception ex)
            {
                Log.WriteError(ex);
            }
            return doubleValue;
        }
        
        /// Helper method to parse a boolean value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>bool value</returns>
        public static bool ParseBool(string value)
        {
            bool boolValue = false;
            try
            {
                bool.TryParse(value, out boolValue);
            }
            catch (Exception ex)
            {
                Log.WriteError(ex);
            }
            return boolValue;
        }    
    
    }
}