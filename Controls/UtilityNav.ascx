﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UtilityNav.ascx.cs" Inherits="UserControls_UtilityNav" %>

<div class="bg-dark-gray accessibility">
    <div class="content-wrapper no-padding">
        <a class="accessibility-mode" id="accessibility-mode" href="#">Accessibility Mode <span>Off</span></a>
        <a class="skip-navigation" id="skip-navigation" href="#home_search_box_header">Skip to content</a>
    </div>
</div>
<!-- RESOURCES BLOCK -->
<div class="block-resources hide-phone">
    <div class="content-wrapper no-padding">
        <div class="bg-dark-gray">
            <!--<a class="icon icon-wheelchair" href="/accessibility/"><svg><use xlink:href="#icon-wheelchair"/></svg> Accessibility</a> &#183; -->
           <a href="/resources/myresources.aspx" title="My Resources">My Resources (<span id="myResourcesCount">0</span>)</a>  ·
                        <a href="#" title="Frequently Asked Questions">FAQs</a> · 
                        <a href="#" title="Contact">Contact</a>
        </div>
    </div>
</div>

<header class="banner" id="banner" role="banner">
    <div class="grid">
        <div class="row-12">
            <div class="column-6 logo" id="logo" title="Social Security Logo">
                <a href="/default.aspx">
                    <asp:Literal ID="ltrLogoText" runat="server"></asp:Literal>                    
                </a>
            </div>
            <!-- END COLUMN -->
            <div class="column-6 align-right hide-phone">
                <div class="search-box" id="search-box" role="search">
                    <input type="hidden" name="affiliate" value="ssa" />
                    <label class="hide" for="q">Search Terms</label>
                    <input class="usagov-search-autocomplete" type="search" name="query" id="q" autocomplete="off" placeholder="Search..." alt="Search" />
                    <span class=" nav-facade-active" id="nav-search-in">
                        <span class="nav-down-arrow nav-sprite"></span>
                        <select title="Search menu" class="searchSelect" id="searchDropdownBox" name="search_category">
                            <option value="This Site" title="This Site">This Site</option>
                            <option value="Library Resources" title="Library Resources" selected="selected">Library Resources</option>
                            <option value="SSA Reports" title="SSA Reports">SSA Reports</option>
                        </select>
                    </span>
                </div>
            </div>
            <!-- END COLUMN -->
        </div>
        <!-- END ROW -->
    </div>
    <!-- END WRAPPER -->
</header>


