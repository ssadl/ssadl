﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Ektron.Cms.Content;
using Ektron.Cms.Common;


namespace SSADL.CMS
{
    /// <summary>
    /// Summary description for SiteDataManager
    /// </summary>
    public class SiteDataManager
    {
        private static ContentTypeManager<News> newsContentTypeMngr = new ContentTypeManager<News>();
        private static ContentTypeManager<FAQs> faqsContentTypeMngr = new ContentTypeManager<FAQs>();
        private static ContentTypeManager<RightColumn> rightColumnContentTypeMngr = new ContentTypeManager<RightColumn>();

        public SiteDataManager()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        
        /// <summary>
        /// This method is used to get latest news sorted by date field
        /// </summary>
        /// <param name="refreshCache"></param>
        /// <returns></returns>
        public static List<ContentType<News>> GetLatestNews(bool refreshCache = false)
        {
            List<ContentType<News>> newsList = null;

            String cacheKey = String.Format("SSADL:SiteDataManager:GetLatestNews:CacheBase");
            if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentType<News>>)HttpRuntime.Cache[cacheKey];

            long newsSmartFormId = ConfigHelper.GetValueLong("NewsSmartFormId");
            ContentCriteria cc = new ContentCriteria();
            cc.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, newsSmartFormId);
            cc.PagingInfo = new Ektron.Cms.PagingInfo(EktUtility.GetCustomApiPageSize());
            newsList = newsContentTypeMngr.GetList(cc);
            if (newsList != null && newsList.Any())
            {
                var sortedList = newsList.OrderBy(x => x.SmartForm.Date).Reverse().ToList();
                ApplicationCache.Insert<List<ContentType<News>>>(cacheKey, sortedList, CacheDuration.For12Hr);
                return sortedList;
            }
            return newsList;
        }

        /// <summary>
        /// This method is used to get news article data
        /// </summary>
        /// <param name="refreshCache"></param>
        /// <returns></returns>
        public static ContentType<News> GetNewsById(long contentId, bool refreshCache = false)
        {
            ContentType<News> newsData = null;

            String cacheKey = String.Format("SSADL:SiteDataManager:GetNewsById={0}:CacheBase", contentId);
            if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (ContentType<News>)HttpRuntime.Cache[cacheKey];

            newsData = newsContentTypeMngr.GetItem(contentId, true);
            if (newsData != null && newsData.Content != null && newsData.Content.Id > 0)
            {
                ApplicationCache.Insert<ContentType<News>>(cacheKey, newsData, CacheDuration.For12Hr);
            }
            return newsData;
        }

       /// <summary>
       /// This method is used to get news by id list
       /// </summary>
       /// <param name="ids">generic list of ids</param>
       /// <param name="refreshCache">bool</param>
       /// <returns></returns>
        public static List<ContentType<News>> GetNewsbyIds(List<long> ids, bool refreshCache = false)
        {
            List<ContentType<News>> newsList = null;

            string contentIds = "";
            foreach (var id in ids)
                contentIds += id;

            String cacheKey = String.Format("SSADL:SiteDataManager:GetNewsbyIds={0}:CacheBase", contentIds);
            if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentType<News>>)HttpRuntime.Cache[cacheKey];

            long newsSmartFormId = ConfigHelper.GetValueLong("NewsSmartFormId");
            ContentCriteria cc = new ContentCriteria();
            cc.AddFilter(ContentProperty.Id, CriteriaFilterOperator.In, ids);
            cc.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, newsSmartFormId);
            cc.PagingInfo = new Ektron.Cms.PagingInfo(EktUtility.GetCustomApiPageSize());
            newsList = newsContentTypeMngr.GetList(cc);
            if (newsList != null && newsList.Any())
            {
                ApplicationCache.Insert<List<ContentType<News>>>(cacheKey, newsList, CacheDuration.For12Hr);
            }
            return newsList;
        }

        /// <summary>
        /// This method is used to get all FAQs
        /// </summary>
        /// <param name="refreshCache"></param>
        /// <returns></returns>
        public static List<ContentType<FAQs>> GetFAQsList(bool refreshCache = false)
        {
            List<ContentType<FAQs>> faqsList = null;

            String cacheKey = String.Format("SSADL:SiteDataManager:GetFAQsList:CacheBase");
            if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentType<FAQs>>)HttpRuntime.Cache[cacheKey];

            long faqSmartFormId = ConfigHelper.GetValueLong("FAQsSmartFormId");
            ContentCriteria cc = new ContentCriteria();
            cc.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, faqSmartFormId);
            cc.PagingInfo = new Ektron.Cms.PagingInfo(EktUtility.GetCustomApiPageSize());
            faqsList = faqsContentTypeMngr.GetList(cc);
            if (faqsList != null && faqsList.Any())
            {
                ApplicationCache.Insert<List<ContentType<FAQs>>>(cacheKey, faqsList, CacheDuration.For12Hr);
            }
            return faqsList;
        }

        /// <summary>
        /// This method is used to FAQs by ids
        /// </summary>
        /// <param name="ids">generic list of long</param>
        /// <param name="refreshCache"></param>
        /// <returns></returns>
        public static List<ContentType<FAQs>> GetFAQsByIds(List<long> ids, bool refreshCache = false)
        {
            List<ContentType<FAQs>> faqsList = null;

            string contentIds = "";
            foreach (var id in ids)
                contentIds += id;

            String cacheKey = String.Format("SSADL:SiteDataManager:GetFAQsByIds={0}:CacheBase", contentIds);
            if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentType<FAQs>>)HttpRuntime.Cache[cacheKey];

            long faqSmartFormId = ConfigHelper.GetValueLong("FAQsSmartFormId");
            ContentCriteria cc = new ContentCriteria();
            cc.AddFilter(ContentProperty.Id, CriteriaFilterOperator.In, ids);
            cc.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, faqSmartFormId);
            cc.PagingInfo = new Ektron.Cms.PagingInfo(EktUtility.GetCustomApiPageSize());
            faqsList = faqsContentTypeMngr.GetList(cc);
            if (faqsList != null && faqsList.Any())
            {
                ApplicationCache.Insert<List<ContentType<FAQs>>>(cacheKey, faqsList, CacheDuration.For12Hr);
            }
            return faqsList;
        }

        /// <summary>
        /// This method is used to get FAQs by Collection Id
        /// </summary>
        /// <param name="CollectionId"></param>
        /// <param name="refreshCache"></param>
        /// <returns></returns>
        public static List<ContentType<FAQs>> GetFAQsbyCollectionId(long CollectionId, bool refreshCache = false)
        {
            List<ContentType<FAQs>> faqsList = null;

            String cacheKey = String.Format("SSADL:SiteDataManager:GetFAQsbyCollectionId={0}:CacheBase", CollectionId);
            if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (List<ContentType<FAQs>>)HttpRuntime.Cache[cacheKey];

            long faqSmartFormId = ConfigHelper.GetValueLong("FAQsSmartFormId");
            ContentCollectionCriteria cc = new ContentCollectionCriteria();
            cc.AddFilter(CollectionId);
            cc.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, faqSmartFormId);
            cc.PagingInfo = new Ektron.Cms.PagingInfo(EktUtility.GetCustomApiPageSize());
            faqsList = faqsContentTypeMngr.GetList(cc);
            if (faqsList != null && faqsList.Any())
            {
                ApplicationCache.Insert<List<ContentType<FAQs>>>(cacheKey, faqsList, CacheDuration.For12Hr);
            }
            return faqsList;
        }

        /// <summary>
        /// This method is used to get right column content by id
        /// </summary>
        /// <param name="contentId"></param>
        /// <param name="refreshCache"></param>
        /// <returns></returns>
        public static ContentType<RightColumn> GetRightColumnContentById(long contentId, bool refreshCache = false)
        {
            ContentType<RightColumn> contentData = null;

            String cacheKey = String.Format("SSADL:SiteDataManager:GetRightColumnContentById={0}:CacheBase", contentId);
            if (HttpRuntime.Cache[cacheKey] != null && refreshCache == false) return (ContentType<RightColumn>)HttpRuntime.Cache[cacheKey];

            long rightSideContentSmartFormId = ConfigHelper.GetValueLong("RightColumnSFId");
            ContentCriteria cc = new ContentCriteria();
            cc.AddFilter(ContentProperty.XmlConfigurationId, CriteriaFilterOperator.EqualTo, rightSideContentSmartFormId);
            cc.AddFilter(ContentProperty.Id, CriteriaFilterOperator.EqualTo, contentId);
            var contentList = rightColumnContentTypeMngr.GetList(cc);
            if (contentList != null && contentList.Any())
            {
                contentData = contentList.FirstOrDefault();
                ApplicationCache.Insert<ContentType<RightColumn>>(cacheKey, contentData, CacheDuration.For12Hr);
            }
            return contentData;
        }
    
    }
}