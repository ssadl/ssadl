﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SSADL.CMS
{
    using System.Xml.Serialization;

    // 
    // This source code was auto-generated by xsd, Version=4.0.30319.33440.
    // 


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Staff
    {

        private string nameField;

        private string titleField;

        private string roleField;

        private StaffImage imageField;

        private bool activeField;

        public Staff()
        {
            this.activeField = false;
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string Role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }

        /// <remarks/>
        public StaffImage Image
        {
            get
            {
                return this.imageField;
            }
            set
            {
                this.imageField = value;
            }
        }

        /// <remarks/>
        public bool Active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class StaffImage
    {

        private imgDesignType imgField;

        /// <remarks/>
        public imgDesignType img
        {
            get
            {
                return this.imgField;
            }
            set
            {
                this.imgField = value;
            }
        }
    }

    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //public partial class imgDesignType
    //{

    //    private string srcField;

    //    private string altField;

    //    private string heightField;

    //    private string widthField;

    //    private ImgAlign alignField;

    //    private bool alignFieldSpecified;

    //    private string borderField;

    //    private string hspaceField;

    //    private string vspaceField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute(DataType = "anyURI")]
    //    public string src
    //    {
    //        get
    //        {
    //            return this.srcField;
    //        }
    //        set
    //        {
    //            this.srcField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string alt
    //    {
    //        get
    //        {
    //            return this.altField;
    //        }
    //        set
    //        {
    //            this.altField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string height
    //    {
    //        get
    //        {
    //            return this.heightField;
    //        }
    //        set
    //        {
    //            this.heightField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string width
    //    {
    //        get
    //        {
    //            return this.widthField;
    //        }
    //        set
    //        {
    //            this.widthField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public ImgAlign align
    //    {
    //        get
    //        {
    //            return this.alignField;
    //        }
    //        set
    //        {
    //            this.alignField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool alignSpecified
    //    {
    //        get
    //        {
    //            return this.alignFieldSpecified;
    //        }
    //        set
    //        {
    //            this.alignFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string border
    //    {
    //        get
    //        {
    //            return this.borderField;
    //        }
    //        set
    //        {
    //            this.borderField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
    //    public string hspace
    //    {
    //        get
    //        {
    //            return this.hspaceField;
    //        }
    //        set
    //        {
    //            this.hspaceField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
    //    public string vspace
    //    {
    //        get
    //        {
    //            return this.vspaceField;
    //        }
    //        set
    //        {
    //            this.vspaceField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    //[System.SerializableAttribute()]
    //public enum ImgAlign
    //{

    //    /// <remarks/>
    //    top,

    //    /// <remarks/>
    //    middle,

    //    /// <remarks/>
    //    bottom,

    //    /// <remarks/>
    //    left,

    //    /// <remarks/>
    //    right,
    //}
}
